package com.khoa.netflixclonemk.menu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.khoa.netflixclonemk.R
import com.khoa.netflixclonemk.comingsoon.ComingSoonFragment
import com.khoa.netflixclonemk.databinding.ActivityMenuBinding
import com.khoa.netflixclonemk.fgdownload.DownloadFragment
import com.khoa.netflixclonemk.home.HomeFragment
import com.khoa.netflixclonemk.more.MoreFragment
import com.khoa.netflixclonemk.search.SearchFragment

@Suppress("DEPRECATION")
class MenuActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMenuBinding
    private var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        fragment = HomeFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout,fragment as HomeFragment)
            .commit()
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.home -> {
                fragment = HomeFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frameLayout, fragment as HomeFragment)
                    .commit()
            }
            R.id.search -> {
                fragment = SearchFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frameLayout,fragment as SearchFragment)
                    .commit()
            }
            R.id.coming ->{
                fragment = ComingSoonFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frameLayout,fragment as ComingSoonFragment)
                    .commit()
            }
            R.id.download ->{
                fragment = DownloadFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frameLayout,fragment as DownloadFragment)
                    .commit()
            }
            R.id.more ->{
                fragment = MoreFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frameLayout,fragment as MoreFragment)
                    .commit()
            }
        }
        return true
    }

    //Lưu ý bottomnavigation không thể nằm trong fragment đc chỉ nằm trong Activity

}