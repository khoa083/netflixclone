package com.khoa.netflixclonemk.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.GridLayoutManager
import com.khoa.netflixclonemk.R
import com.khoa.netflixclonemk.databinding.ActivityProfileBinding

@Suppress("DEPRECATION")
class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding
    private var listProfile: List<Profile>? = null
    private var profileAdapter: ProfileAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        handleDataProfile()
    }

    private fun handleDataProfile() {
        listProfile = listOf(
            Profile(R.drawable.profile1,"Khoa"),
            Profile(R.drawable.profile2,"Tuấn"),
            Profile(R.drawable.profile3,"Huy"),
            Profile(R.drawable.profile4,"Hưng"),
            Profile(R.drawable.add_profile,"..."),
        )
        binding.rvProfile.apply {
            val layoutParam = GridLayoutManager(this@ProfileActivity, 2)
            layoutManager = layoutParam
            profileAdapter = ProfileAdapter(listProfile?: listOf())
            adapter = profileAdapter
        }
    }
}