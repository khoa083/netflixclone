package com.khoa.netflixclonemk.profile

data class Profile(val image: Int, val title: String)
