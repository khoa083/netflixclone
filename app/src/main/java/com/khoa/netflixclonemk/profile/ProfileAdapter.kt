package com.khoa.netflixclonemk.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.khoa.netflixclonemk.databinding.ItemProfileBinding

class ProfileAdapter(private val list: List<Profile>): RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProfileViewHolder = ProfileViewHolder(ItemProfileBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) = holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class ProfileViewHolder(private val binding: ItemProfileBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(profile: Profile){
            binding.ivProfile.setImageDrawable(ContextCompat.getDrawable(binding.ivProfile.context, profile.image))
            binding.tvProfileName.text = profile.title
        }
    }
}