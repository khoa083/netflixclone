package com.khoa.netflixclonemk.search

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.khoa.netflixclonemk.R
import com.khoa.netflixclonemk.databinding.FragmentSearchBinding
import com.khoa.netflixclonemk.play.PlayVideo
import java.util.Locale
import java.util.Objects

@Suppress("DEPRECATION", "PrivatePropertyName")
class SearchFragment : Fragment() {

    private lateinit var binding: FragmentSearchBinding
    private var listFilm: List<Film>? = null
    private var filmAdapter: FilmAdapter? = null
//    private var t1: TextToSpeech? = null
    private val REQUEST_CODE_SPEECH_INPUT = 1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(layoutInflater)
        return binding.root
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        handleDataFilm()
        handleSpeechToText()
    }

    private fun handleSpeechToText() {
        binding.ivMicro.setOnClickListener {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault()
            )
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text")
            try {
                startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT)
            } catch (e: Exception) {
                Toast
                    .makeText(
                        activity, " " + e.message,
                        Toast.LENGTH_SHORT
                    )
                    .show()
            }

        }
    }
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {
                val res: ArrayList<String> =
                    data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS) as ArrayList<String>
                binding.edtSearch.setText(
                    Objects.requireNonNull(res)[0]
                )
            }
        }
    }
// text to speech
//    private fun setUpListener() {
//        t1 = TextToSpeech(applicationContext) { status ->
//            if (status != TextToSpeech.ERROR) {
////                t1?.setLanguage(Locale.UK)
//                t1?.setLanguage(Locale.forLanguageTag("vi-VN"))
//            }
//        }
//
//        binding.tvMicro.setOnClickListener {
//            val toSpeak: String = binding.edtSearch.getText().toString()
//            binding.edtSearch.setText(toSpeak)
//            Toast.makeText(applicationContext, toSpeak, Toast.LENGTH_SHORT).show()
//            t1?.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null)
//        }
//    }

    private fun handleDataFilm() {
        listFilm = listOf(
            Film(R.drawable.image, "Explained",R.raw.diablo_4),
            Film(R.drawable.image1, "Into The Night",R.raw.godzilla),
            Film(R.drawable.image2, "Locke & Key",R.raw.meg_2),
            Film(R.drawable.image3, "The Queens Gambit"),
            Film(R.drawable.image4, "The Crown"),
            Film(R.drawable.image5, "Altered Carbon"),
            Film(R.drawable.image6, "Another Life"),
            Film(R.drawable.image7, "The Umbrella Academy"),
            Film(R.drawable.image8, "Sense8"),
            Film(R.drawable.image9, "The Rain"),
            Film(R.drawable.image10, "You"),
            Film(R.drawable.image11, "Snow Piercer"),
            Film(R.drawable.image12, "The Universe"),
            Film(R.drawable.image13, "The Last Kingdom"),
            Film(R.drawable.image14, "Money Heist"),
            Film(R.drawable.image15, "The Set Up"),
            Film(R.drawable.image16, "Ozark"),
            Film(R.drawable.image17, "Your Excellency"),
            Film(R.drawable.image18, "Citation"),
            Film(R.drawable.image19, "Breaking Bad"),
            Film(R.drawable.image20, "The Governor")
        )

//        val snapHelper: SnapHelper = PagerSnapHelper()
//        snapHelper.attachToRecyclerView(binding.rvFilm)

        binding.rvFilm.apply {
            val layoutParam = GridLayoutManager(activity, 1)
            layoutManager = layoutParam
            filmAdapter = FilmAdapter()
            filmAdapter?.setupData(listFilm ?: listOf())
            adapter = filmAdapter

            binding.edtSearch.doOnTextChanged { text, _, _, _ -> //start, before, count
                filmAdapter?.filter?.filter(text.toString())
            }
        }
        filmAdapter?.onClickItem = {
//            Toast.makeText(requireActivity(), it.title,Toast.LENGTH_SHORT).show()
            Snackbar.make(binding.root, it.title,800).show()
            Log.e("Khoa",it.title)


            val intent = Intent(requireActivity(), PlayVideo::class.java)
            intent.putExtra("key",it)
            startActivity(intent)

        }
    }

    @Deprecated("Deprecated in Java", ReplaceWith("finish()"))
    fun onBackPressed() {
    }
}