package com.khoa.netflixclonemk.search

import java.io.Serializable

data class Film(val image: Int, val title: String, val fileFilm: Int? = null):Serializable //Serializable bien dich ve dang file