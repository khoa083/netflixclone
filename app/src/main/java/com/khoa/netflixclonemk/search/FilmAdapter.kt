package com.khoa.netflixclonemk.search

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.khoa.netflixclonemk.databinding.ItemFilmBinding

class FilmAdapter: RecyclerView.Adapter<FilmAdapter.FilmViewHolder>(), Filterable {

    var onClickItem: ((Film) -> Unit)? = null
    private val listDataFilm = ArrayList<Film>()
    private var listDateFilmFiltered = ArrayList<Film>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmViewHolder
            = FilmViewHolder(ItemFilmBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = listDateFilmFiltered.size

    override fun onBindViewHolder(holder: FilmViewHolder, position: Int) = holder.bind(listDateFilmFiltered[position])


    inner class FilmViewHolder(private val binding: ItemFilmBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(film: Film){
            binding.ivFilm.setImageDrawable(ContextCompat.getDrawable(binding.ivFilm.context, film.image))
            binding.tvTitle.text = film.title
            binding.root.setOnClickListener{
                onClickItem?.invoke(film) //invoke khi click xong se xac thuc da click
            }
        }
    }

    fun setupData(newData: List<Film>) {
        listDataFilm.clear().also { //also luon dung
            listDataFilm.addAll(newData)
            filter.filter("")
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                listDateFilmFiltered = if (charString.isEmpty()) {
                    listDataFilm
                } else {
                    val filteredList = ArrayList<Film>()
                    for (film in listDataFilm) {
                        if ((film.title).lowercase().contains(charString.lowercase()))
                            filteredList.add(film)
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = listDateFilmFiltered
                return filterResults
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                listDateFilmFiltered = results?.values as ArrayList<Film>
                notifyDataSetChanged()
            }

        }
    }

}