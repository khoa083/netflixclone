package com.khoa.netflixclonemk.splash

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.khoa.netflixclonemk.R
import com.khoa.netflixclonemk.databinding.ActivitySplashBinding
import com.khoa.netflixclonemk.menu.MenuActivity

@Suppress("DEPRECATION")
@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Glide.with(this).load(R.drawable.img_splash).into(binding.ivSplash)


        Handler(Looper.getMainLooper()).postDelayed({
//            startActivity(Intent(this,SearchActivity::class.java))
//            startActivity(Intent(this,HomeActivity::class.java))
            startActivity(Intent(this, MenuActivity::class.java))
        },4450)
    }
}