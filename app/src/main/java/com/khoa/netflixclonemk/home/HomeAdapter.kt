package com.khoa.netflixclonemk.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.khoa.netflixclonemk.databinding.ItemHomeBinding

class HomeAdapter(private val list: List<Home>): RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder
        = HomeViewHolder(ItemHomeBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) = holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class HomeViewHolder(private val binding: ItemHomeBinding):RecyclerView.ViewHolder(binding.root){
        @SuppressLint("NotifyDataSetChanged")
        fun bind(home: Home){
            binding.tvTitle.text = home.title

            binding.rvSubListHome.layoutManager = LinearLayoutManager(binding.root.context,LinearLayoutManager.HORIZONTAL,false)
            binding.rvSubListHome.adapter = SubHomeAdapter(home.subHome)
        }
    }

}