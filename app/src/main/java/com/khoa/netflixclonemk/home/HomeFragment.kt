package com.khoa.netflixclonemk.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.khoa.netflixclonemk.R
import com.khoa.netflixclonemk.databinding.FragmentHomeBinding

@Suppress("DEPRECATION")
class HomeFragment: Fragment() {

    private lateinit var binding: FragmentHomeBinding

    private var listHome: List<Home>? = null

    private var listPreviewsSubHome: List<SubHome>? = null
    private var listPopularSubHome: List<SubHome>? = null
    private var listTrendingSubHome: List<SubHome>? = null
    private var listTopSubHome: List<SubHome>? = null
    private var listMyListSubHome: List<SubHome>? = null
    private var listAfricanSubHome: List<SubHome>? = null
    private var listOriginalsSubHome: List<SubHome>? = null

    private var homeAdapter: HomeAdapter? = null

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        handleDataHome()
    }

    private fun handleDataHome() {

        listPreviewsSubHome = listOf(
            SubHome(R.drawable.img1_radius_home),
            SubHome(R.drawable.img2_radius_home),
            SubHome(R.drawable.img3_radius_home),
            SubHome(R.drawable.img4_radius_home)
        )

        listPopularSubHome = listOf(
            SubHome(R.drawable.img_sub_home1),
            SubHome(R.drawable.img_sub_home2),
            SubHome(R.drawable.img_sub_home3),
            SubHome(R.drawable.img_sub_home4),
        )

        listTrendingSubHome = listOf(
            SubHome(R.drawable.img_sub_home5),
            SubHome(R.drawable.img_sub_home6),
            SubHome(R.drawable.img_sub_home7),
            SubHome(R.drawable.img_sub_home8)
        )

        listTopSubHome = listOf(
            SubHome(R.drawable.img_sub_home9),
            SubHome(R.drawable.img_sub_home10),
            SubHome(R.drawable.img_sub_home11),
            SubHome(R.drawable.img_sub_home12)
        )

        listMyListSubHome = listOf(
            SubHome(R.drawable.img_sub_home13),
            SubHome(R.drawable.img_sub_home14),
            SubHome(R.drawable.img_sub_home15),
            SubHome(R.drawable.img_sub_home16)
        )

        listAfricanSubHome = listOf(
            SubHome(R.drawable.img_sub_home17),
            SubHome(R.drawable.img_sub_home18),
            SubHome(R.drawable.img_sub_home19),
            SubHome(R.drawable.img_sub_home20)
        )

        listOriginalsSubHome = listOf(
            SubHome(R.drawable.img_sub_home_large1),
            SubHome(R.drawable.img_sub_home_large2),
            SubHome(R.drawable.img_sub_home_large3)
        )

        listHome = listOf(
            Home("Previews", listPreviewsSubHome?: listOf()),
            Home("Popular", listPopularSubHome?: listOf()),
            Home("Trending", listTrendingSubHome?: listOf()),
            Home("Top 10 in Nigeria Today", listTopSubHome?: listOf()),
            Home("Netflix Originals", listOriginalsSubHome?: listOf()),
            Home("My list", listMyListSubHome?: listOf()),
            Home("African Movies", listAfricanSubHome?: listOf())
        )

        binding.rvListHome.apply {
            layoutManager = GridLayoutManager(requireContext(), 1)
            homeAdapter = HomeAdapter(listHome?: listOf())
            adapter = homeAdapter
        }
    }
}