package com.khoa.netflixclonemk.home

data class Home(val title: String, val subHome: List<SubHome>)

