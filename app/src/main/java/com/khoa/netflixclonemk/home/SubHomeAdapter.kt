package com.khoa.netflixclonemk.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.khoa.netflixclonemk.databinding.ItemSubHomeBinding

class SubHomeAdapter(private val list: List<SubHome>): RecyclerView.Adapter<SubHomeAdapter.SubHomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubHomeViewHolder
        = SubHomeViewHolder(ItemSubHomeBinding.inflate(LayoutInflater.from(parent.context),parent,false))


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SubHomeViewHolder, position: Int) = holder.bind(list[position])

    inner class SubHomeViewHolder(private val binding: ItemSubHomeBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(subHome: SubHome){
            binding.ivSubHome.setImageDrawable(ContextCompat.getDrawable(binding.ivSubHome.context,subHome.images))
        }
    }

}