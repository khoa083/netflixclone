package com.khoa.netflixclonemk.comingsoon

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.khoa.netflixclonemk.databinding.ItemComingsoonBinding

class ComingSoonAdapter(private val array: List<ComingSoon>): RecyclerView.Adapter<ComingSoonAdapter.ComingSoonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComingSoonViewHolder
            = ComingSoonViewHolder(ItemComingsoonBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun getItemCount(): Int = array.size

    override fun onBindViewHolder(holder: ComingSoonViewHolder, position: Int) = holder.bind(array[position])

    inner class ComingSoonViewHolder(private val binding: ItemComingsoonBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(comingSoon: ComingSoon){
            binding.ivBannerComingSoon.setImageDrawable(ContextCompat.getDrawable(binding.ivBannerComingSoon.context,comingSoon.images))
        }
    }

}