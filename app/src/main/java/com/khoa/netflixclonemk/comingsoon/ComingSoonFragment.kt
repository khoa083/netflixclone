package com.khoa.netflixclonemk.comingsoon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.khoa.netflixclonemk.R
import com.khoa.netflixclonemk.databinding.FragmentComingsoonBinding

class ComingSoonFragment: Fragment() {

    private lateinit var binding: FragmentComingsoonBinding
    private var arrayComingSoon: ArrayList<ComingSoon> = ArrayList()
    private var comingSoonAdapter: ComingSoonAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentComingsoonBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleDataComingSoon()
    }

    private fun handleDataComingSoon() {
        arrayComingSoon.add(ComingSoon(R.drawable.img_comingsoon3))
        arrayComingSoon.add(ComingSoon(R.drawable.img_comingsoon1))
        arrayComingSoon.add(ComingSoon(R.drawable.img_comingsoon2))

        binding.rvListComingSoon.apply {
            layoutManager = GridLayoutManager(requireContext(),1)
            comingSoonAdapter = ComingSoonAdapter(arrayComingSoon)
            adapter = comingSoonAdapter
        }
    }
}