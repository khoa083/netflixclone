package com.khoa.netflixclonemk.more

import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.khoa.netflixclonemk.databinding.FragmentMoreBinding

class MoreFragment:Fragment() {
    private lateinit var binding: FragmentMoreBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMoreBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleTextUnderline()
    }

    private fun handleTextUnderline() {
        val str = binding.tvUnderline.text
        val spannableString = SpannableString(str.toString())
        spannableString.setSpan(UnderlineSpan(),0,spannableString.length,0)
        binding.tvUnderline.text = spannableString
    }

}